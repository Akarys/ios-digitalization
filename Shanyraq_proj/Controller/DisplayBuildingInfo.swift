//
//  DisplayBuildingInfo.swift
//  Shanyraq_proj
//
//  Created by Akarys Turganbekuly on 13.08.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import Foundation
import UIKit
import JSONRPCKit
import APIKit
import SwiftyJSON


class DisplayBuildingInfo: UIViewController{
    var idwka = jkID
    var
    modelSchet = String(),
    oblasT = String(),
    goroD = String(),
    obweeKolvoipu = String(),
    dataPodklKsem = String(),
    dataPodklKksk = String(),
    mikroraioN = String(),
    nomerdoma = String(),
    kolvoKvartiR = String(),
    OPUt = String(),
    ATp = String(),
    dataUstanovkI = String(),
    dataPodklKShanyraQ = String(),
    dataPodklKeksk = String(),
    otvetstvenniDolJ = String(),
    ovetstvennifio = String(),
    otvetstvennitel = String(),
    semOrgaN = String(),
    semfio = String(),
    semTeL = String()
    
    @IBOutlet weak var ModelSchet4ika: UIImageView!
    @IBOutlet weak var Oblast: UILabel!
    @IBOutlet weak var Gorod: UILabel!
    @IBOutlet weak var ObwKolipu: UILabel!
    @IBOutlet weak var EisKSem: UILabel!
    @IBOutlet weak var EisKKsk: UILabel!
    @IBOutlet weak var Mikroraio: UILabel!
    @IBOutlet weak var NomerDoma: UILabel!
    @IBOutlet weak var KolvoKvartir: UILabel!
    @IBOutlet weak var Oput: UILabel!
    @IBOutlet weak var Atp: UILabel!
    @IBOutlet weak var DataUstanovki: UILabel!
    @IBOutlet weak var PodklKShanyraq: UILabel!
    @IBOutlet weak var PodklKeKSK: UILabel!
    @IBOutlet weak var OtvetDolzh: UILabel!
    @IBOutlet weak var OtvetFIO: UILabel!
    @IBOutlet weak var OtvetTel: UILabel!
    @IBOutlet weak var SemOrganizaciya: UILabel!
    @IBOutlet weak var SemFIO: UILabel!
    @IBOutlet weak var SemTel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        getResponse(o_method: "search_read", o_model: "shanyraq.digitalization", o_domain: [["building_id", "=", 210]], o_fields: ["region","city","ipu_total","date_eiis_sem","date_eiis_ksk", "mikro_district", "apartment_number", "total_apartment", "oput","atp","date_ustanovka","date_shanyraq", "date_ksk", "responsible_position", "responsible_name", "responsible_number", "sem_name", "sem_head", "sem_number"]) { (result) in
            
            print(result)
            
            
            
            let Oblast = result[0]["region"].string
            let Gorod = result[0]["city"].string
            let ObweeKolvoIPU = result[0]["ipu_total"].string
            let DataPodklKSEM = result[0]["date_eiis_sem"].string
            let DataPodklKKSK = result[0]["date_eiis_ksk"].string
            let Mikroraion = result[0]["mikro_district"].string
            let NomerDoma = result[0]["apartment_number"].string
            let KolvoKvartit = result[0]["total_apartment"].string
            let oPUT = result[0]["oput"].string
            let aTP = result[0]["atp"].string
            let DataUstanovki = result[0]["date_ustanovka"].string
            let DataPodklKShanyraq = result[0]["date_shanyraq"].string
            let DataPodklKeKSK = result[0]["date_ksk"].string
            let OtvetstvenniDolj = result[0]["responsible_position"].string
            let OvetstvenniFIO = result[0]["responsible_name"].string
            let OtvetstvenniTEL = result[0]["responsible_number"].string
            let SemOrgan = result[0]["sem_name"].string
            let SemFIO = result[0]["sem_head"].string
            let SemTel = result[0]["sem_number"].string
    
            self.Oblast.text = Oblast
            self.Gorod.text = Gorod
            self.ObwKolipu.text = ObweeKolvoIPU
            self.EisKSem.text = DataPodklKSEM
            self.EisKKsk.text = DataPodklKKSK
            self.Mikroraio.text = Mikroraion
            self.NomerDoma.text = NomerDoma
            self.KolvoKvartir.text = KolvoKvartit
            self.Oput.text = oPUT
            self.Atp.text = aTP
            self.DataUstanovki.text = DataUstanovki
            self.PodklKShanyraq.text = DataPodklKShanyraq
            self.PodklKeKSK.text = DataPodklKeKSK
            self.OtvetDolzh.text = OtvetstvenniDolj
            self.OtvetFIO.text = OvetstvenniFIO
            self.OtvetTel.text = OtvetstvenniTEL
            self.SemOrganizaciya.text = SemOrgan
            self.SemFIO.text = SemFIO
            self.SemTel.text = SemTel
            
        }
    }
}
