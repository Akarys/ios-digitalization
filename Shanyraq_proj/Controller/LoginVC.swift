//
//  LoginVC.swift
//  Shanyraq_proj
//
//  Created by Akarys Turganbekuly on 08.08.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import UIKit
import JSONRPCKit
import APIKit
import SwiftyJSON

var user_id: Any?

class LoginVC: UIViewController {
    
    
    
    @IBOutlet weak var usernameTextField: DesignableUITextField!
    @IBOutlet weak var passwordTextField: DesignableUITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func loginBtnPressed(_ sender: Any) {
     let username: String = usernameTextField.text!
     let password: String = passwordTextField.text!
        
        odooAuthenticate(db: "shanyraqprod", login: username, password: password) { (result)->() in
            //print(result)
            //print(result["uid"].int!)
            user_id = result["uid"].int!
            print(user_id!)
            if let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DigitalizationVC") as? DigitalizationVC {
                self.present(destination, animated: true, completion: nil)

        }
    }
}
}
