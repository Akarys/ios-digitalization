//
//  DigitalizationVC.swift
//  Shanyraq_proj
//
//  Created by Akarys Turganbekuly on 08.08.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import UIKit
import JSONRPCKit
import APIKit
import SwiftyJSON

var jkID: Int = 0

class DigitalizationVC: UIViewController {
   
    var data = [resUser]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        
        getResponse(o_method: "search_read", o_model: "res.users", o_domain:[["id", "=", user_id]], o_fields: ["name","partner_id"]) { (result) -> () in
            //print(result)
            if result != JSON.null {
                let partner_id = result[0]["partner_id"][0].int!
                
                getResponse(o_method: "search_read", o_model: "res.partner", o_domain: [["id", "=", partner_id]], o_fields: ["name", "kato"], completion: { (result)->() in
                    //print(result)
                    let id = result[0]["kato"][0].int!
                    
                    
                    getResponse(o_method: "search_read", o_model: "property.building", o_domain: [["kato", "=", id]], o_fields: ["name", "region"], completion: { (result)->() in
                        
                        result.forEach({ (temp) in
                            
                            var area:String = "", name:String = "", totID: Int = 0
                            
                            if (temp.1["region"].string != nil) {
                                area = temp.1["region"].string!
                            }
                            if (temp.1["name"].string != nil) {
                                name = temp.1["name"].string!
                            }
                            if (temp.1["id"].int != nil) {
                                totID = temp.1["id"].int!
                            }
                            let newModel = resUser(area: area, name: name, id: totID)
                            self.data.append(newModel)
                        })
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                        print(self.data)
                    })
                   
                })
                
            }
            
        }
    }
}

extension DigitalizationVC: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "digitcell", for: indexPath) as! DigitalizationCell
     let item = data[indexPath.row]
        cell.jkXMLReponse.text = item.area
        cell.mkrName.text = item.name
        return cell
    }
    
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            //print(self.data[indexPath.row].id)
        jkID = self.data[indexPath.row].id
            print(jkID)
        let Storyboard = UIStoryboard(name: "Main", bundle: nil)
        let CWS = Storyboard.instantiateViewController(withIdentifier: "DisplayBuildingInfo") as! DisplayBuildingInfo
        self.navigationController?.pushViewController(CWS, animated: true)
    }
}
