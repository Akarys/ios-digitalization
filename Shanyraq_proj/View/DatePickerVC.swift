//
//  DatePickerVC.swift
//  Shanyraq_proj
//
//  Created by Akarys Turganbekuly on 13.08.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import Foundation
import UIKit

class DatePickerVC: UIViewController {
    
    @IBOutlet weak var SelectDate: UILabel!
    @IBOutlet weak var DatePicker: UIDatePicker!
    @IBOutlet weak var SaveDate: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func SaveBtnTapped(_ sender: UIButton) {
         dismiss(animated: true, completion: nil )
    }
}
