//
//  buildingCell.swift
//  Shanyraq_proj
//
//  Created by Akarys Turganbekuly on 10.08.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import Foundation
import UIKit

class BuildingCell: UITableViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var responseText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
