//
//  DigitalizationCell.swift
//  Shanyraq_proj
//
//  Created by Akarys Turganbekuly on 03.08.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import Foundation
import UIKit

class DigitalizationCell: UITableViewCell {
    
    @IBOutlet weak var jkXMLReponse: UILabel!
    @IBOutlet weak var hImage: UIImageView!
    @IBOutlet weak var mkrName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

