//
//  JSONRPCStructure.swift
//  Shanyraq_proj
//
//  Created by Akarys Turganbekuly on 08.08.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import UIKit
import JSONRPCKit
import APIKit


let ODOO_URL = "http://shanyraq.valis.kz/"

//MARK:- Request for tables
struct MyServiceRequest<Batch: JSONRPCKit.Batch>: APIKit.Request {
    let batch: Batch

    typealias Response = Batch.Responses
    
    var baseURL: URL {
        return URL(string: ODOO_URL+"web/dataset/call_kw")!
    }

    var method: HTTPMethod {
        return .post
    }
    
    var path: String {
        return "/"
    }
    
    var parameters: Any? {
        return batch.requestObject
    }
    
    func response(from object: Any, urlResponse: HTTPURLResponse) throws -> Response {
        return try batch.responses(from: object)
    }
}

struct RPCRequest: JSONRPCKit.Request {
    typealias Response = Any
    
    //let params:Any?
    
    let o_method:String //"search_read"
    let o_model:String //"res.users"
    let o_domain:Any? // [["id", "=", partner_id],...]
    let o_fields:Any? //["name","partner_id"]
    
    var method: String {
        return "call"
    }
    
    var parameters: Any? {
        return ["kwargs":["context":[]], "args":[o_domain,o_fields], "method":o_method, "model":o_model]
    }
    
    func response(from resultObject: Any) throws -> Response {
        if let response = resultObject as? Response {
            return response
        } else {
            return resultObject
        }
    }
} //all for rest requests

//MARK:- Request for auth
struct AuthenticationServiceRequest<Batch: JSONRPCKit.Batch>: APIKit.Request {
    let batch: Batch
    
    typealias Response = Batch.Responses
    
    var baseURL: URL {
        return URL(string: ODOO_URL+"web/session/authenticate")!
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    var path: String {
        return "/"
    }
    
    var parameters: Any? {
        return batch.requestObject
    }
    
    func response(from object: Any, urlResponse: HTTPURLResponse) throws -> Response {
        return try batch.responses(from: object)
    }
}

struct RPCAuthenticationRequest: JSONRPCKit.Request {
    typealias Response = Any
    
    let o_db:String
    let o_login:String
    let o_password:String
    
    var method: String {
        return "call"
    }
    
    var parameters: Any? {
        return ["db":o_db, "login":o_login, "password":o_password]
    }
    
    func response(from resultObject: Any) throws -> Response {
        if let response = resultObject as? Response {
            return response
        } else {
            return resultObject
        }
    }
} //all for auth


