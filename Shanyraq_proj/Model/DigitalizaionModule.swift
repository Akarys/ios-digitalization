//
//  DigitalizaionModule.swift
//  Shanyraq_proj
//
//  Created by Akarys Turganbekuly on 02.08.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import UIKit

struct assetBuild {
    let id: Int?
    let name: String?
    let measureIds: [Int]?
}
