//
//  assetsBuild.swift
//  Shanyraq
//
//  Created by Akarys Turganbekuly on 30.07.2018.
//  Copyright © 2018 Akarys Turganbekuly. All rights reserved.
//

import Foundation

struct resUser: Decodable {
    let area: String
    let name: String
    let id: Int
    }

struct buildings {
    let oblasT: [String]
    let goroD: [String]
    let obweeKolvoipu: [String]
    let dataPodklKsem: [String]
    let dataPodklKksk: [String]
    let mikroraioN: [String]
    let nomerdoma: [String]
    let kolvoKvartiR: [String]
    let OPUt : [String]
    let ATp: [String]
    let dataUstanovkI: [String]
    let dataPodklKShanyraQ: [String]
    let dataPodklKeksk: [String]
    let otvetstvenniDolJ: [String]
    let ovetstvennifio: [String]
    let otvetstvennitel: [String]
    let semOrgaN: [String]
    let semfio: [String]
    let semTeL: [String]
    
}

struct names {
    let name: [String]
}

